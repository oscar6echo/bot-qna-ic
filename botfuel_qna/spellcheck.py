
import os
import json

import requests as rq


def spellcheck(sentence):
    """
    """

    headers = {
        'app-id': os.environ.get('APP_ID'),
        'app-key': os.environ.get('APP_KEY'),
        'content-type': 'application/json'
    }

    params = {
        'sentence': sentence,
        'key': 'EN_1'
    }

    r = rq.get('https://api.botfuel.io/nlp/spellchecking/',
               headers=headers,
               params=params)

    res = r.content.decode('utf-8')
    res = json.loads(res)
    return res
