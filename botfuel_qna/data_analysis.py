
import numpy as np
import pandas as pd

from IPython.display import display, Markdown


def data_overview(data):
    """
    """
    corpus = data['corpus']
    qnas = data['qnas']

    display(Markdown('Nb groups in corpus: **{}**'.format(len(corpus))))

    dic = {e['name']: len(e['words']) for e in corpus}
    df_corpus = pd.DataFrame(dic, index=['Nb words']).T
    df_corpus = df_corpus.sort_values(by='Nb words', ascending=False)
    display(df_corpus)


    li = [len(e['questions']) for e in qnas]
    m = max(li)
    nbq = sum(li)
    display(Markdown('Nb answers-questions in qnas: **{}**-**{}**'.format(len(qnas), nbq)))


    se_qnas = pd.Series(li)
    ax = se_qnas.hist(figsize=(10, 4), bins=np.arange(m+2) + 0.5)
    ax.set_title('Distribution of Nb questions / answer')
    ax.set_xlabel('Nb of questions / answer')
    ax.set_ylabel('Nb of questions')
    

    return se_qnas
