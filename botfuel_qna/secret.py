
import os


def load_secrets_as_env_variables(path='.env',
                                  verbose=False):
    """
    """
    if path is None:
        path = os.path.join(os.path.expanduser('~'), '.env')

    if verbose:
        print('Loading secrets from {} as env variables'.format(path))

    if os.path.exists(path):
        with open(path, 'r') as f:
            lines = f.readlines()
            lines = [e.replace(' ', '') for e in lines]
            lines = [e.split('\n')[0] for e in lines if e != '']
            for e in lines:
                k, v = e.split('=')
                os.environ[k] = v
                if verbose:
                    print('{}: {}'.format(k, show_secret(v)))


def show_secret(val,
                start=1,
                end=1):
    """
    """
    val = str(val)

    if len(val) > start + end:
        hidden = '*' * (len(val) - start - end)
        return val[:start] + hidden + val[-end:]

    return '*' * len(val)
