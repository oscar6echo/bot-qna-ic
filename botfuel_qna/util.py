

def preview(val,
            start=100,
            end=100):
    """
    """
    val = str(val)

    if len(val) > start + end:
        hidden = '....({} more hidden charactes)....'.format(
            len(val) - start - end)
        return val[:start] + hidden + val[-end:]

    return val
