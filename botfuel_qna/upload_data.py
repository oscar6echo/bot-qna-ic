
import os
import json

import requests as rq

from requests.packages.urllib3.exceptions import InsecureRequestWarning
rq.packages.urllib3.disable_warnings(InsecureRequestWarning)


def upload_dataset(dataset, proxies):
    """
    """

    headers = {
        'app-id': os.environ.get('APP_ID'),
        'app-key': os.environ.get('APP_KEY'),
        'content-type': 'application/json'
    }

    r = rq.post('https://api.botfuel.io/qna/api/v1/bots/import',
                headers=headers,
                data=json.dumps(dataset),
                proxies=proxies,
                verify=False)
    return r
