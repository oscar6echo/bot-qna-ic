
import os
import urllib

import requests as rq


def get_proxies(verbose=False):
    """
    """
    try:
        rq.get('https://google.com')
        proxies = {}

    except:
        login = os.environ.get('login')
        pwd = urllib.parse.quote(os.environ.get('password'))
        dic = {
            'login': login,
            'pwd': pwd,
            'proxy_host': 'proxy-mkt.int.world.socgen',
            'proxy_port': '8080',
        }
        proxies = {
            'http': 'http://{login}:{pwd}@{proxy_host}:{proxy_port}'.format(**dic),
            'https': 'https://{login}:{pwd}@{proxy_host}:{proxy_port}'.format(**dic)
        }

    if verbose:
        print('proxies:', proxies)

    return proxies
