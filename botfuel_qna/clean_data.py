
def remove_html(value):
    """
    """
    if isinstance(value, str):
        s = value.replace('<br>', '')
        s = s.replace('<b>', '')
        s = s.replace('</b>', '')
        return s
    return value
