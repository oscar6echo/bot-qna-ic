
import dotenv from 'dotenv';
import logging from 'simple-node-logger';

const result = dotenv.config()
if (result.error) {
	throw result.error;
}

const log = logging.createSimpleLogger();

let config = {
	botfuelAppId: process.env.APP_ID,
	botfuelAppKey: process.env.APP_KEY,
	MicrosoftAppId: undefined,
	MicrosoftAppPassword: undefined,

	PROXY_HOST: process.env.PROXY_HOST,
	PROXY_LOGIN: process.env.PROXY_LOGIN,
	PROXY_PORT: process.env.PROXY_PORT,
	PROXY_PASSWORD: process.env.PROXY_PASSWORD,

	PROXY_BOOL: process.env.PROXY_BOOL == 'true' ? true : false
};

const proxy = config.PROXY_BOOL ? {
	host: config.PROXY_HOST,
	port: config.PROXY_PORT,
	auth: {
		username: config.PROXY_LOGIN,
		password: encodeURIComponent(config.PROXY_PASSWORD),
	}
} : undefined;

config.proxy = proxy;

log.info('Using ES modules natively in Node.js: http://2ality.com/2017/09/native-esm-node.html');

log.info('config: ');
log.info(config);


export { config };

