
const GREETINGS = ['hello', 'hi', 'good morning', 'good afternoon', 'good day', 'hey'];
const GOODBYES = ['bye', 'bye bye', 'goodbye', 'good bye', 'so long', 'ciao', 'hasta la vista'];

export default {
	KEY: 'EN_1',
	BOT_DID_NOT_UNDERSTAND: 'I did not understand. Please be more explicit.',
	BOT_SAYS_HI: 'Hello, I am the iC user guide bot.\n\nHow can I help you?',
	BOT_ASKS_FOR_FEEDBACK: 'Please leave a feedback to my human supervisor.\n\nHave my answers helped you?',
	BOT_THANKS_FOR_FEEDBACK: 'Ok. Your feedback was recorded. Thank you.',
	BOT_ASKS_FOR_SELECT_ACTION: 'Your question can be understood in several ways.\n\nWhich one did you mean ?',
	BOT_RECALLS_USER_QUESTION: 'You asked:',

	FALLBACK_MESSAGE: 'I have no answers for this question...',

	isGreetings: text => {
		return GREETINGS.some(word => word === text.toLowerCase());
	},

	isGoodbye: text => {
		return GOODBYES.some(word => word === text.toLowerCase());
	},

	strip: text => {
		let text2 = text.replace(/\s\s+/g, ' ');
		text2 = text2.trim();
		return text2
	},
}
