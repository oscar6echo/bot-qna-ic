
import logging from 'simple-node-logger';
import axios from 'axios';

import { config } from './config';

const log = logging.createSimpleLogger();

// const proxy = config.PROXY_BOOL == 'true' ? {
// 	host: config.PROXY_HOST,
// 	port: config.PROXY_PORT,
// 	auth: {
// 		username: config.PROXY_LOGIN,
// 		password: config.PROXY_PASSWORD
// 	}
// } : undefined;
// log.info(`proxy: ${proxy}`);


const asyncHttpRequest = async (requestConfig) => {
	let response;
	try {
		response = await axios.request(requestConfig);
	}
	catch (err) {
		handleError(err);
	}
	return response;
};


const asyncSpellchecking = async (sentence, key) => {
	log.info('start asyncSpellchecking');
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'nlp/spellchecking',
		method: 'get',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		params: {
			sentence: sentence,
			key: key
		},
	};
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}
	let res = await asyncHttpRequest(requestConfig);
	log.info('resolve asyncSpellchecking');
	return res
};


const asyncEntityExtraction = async (sentence) => {
	log.info('start asyncEntityExtraction');
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'nlp/entity-extraction',
		method: 'get',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		params: {
			sentence: sentence
		},
	};
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}

	let res = await asyncHttpRequest(requestConfig);
	log.info('resolve asyncEntityExtraction');
	return res
};


const asyncQnA = async (text) => {
	log.info('start asyncQnA');
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'qna/api/v1/bots/classify',
		method: 'post',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		data: {
			sentence: text
		},
	};
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}

	let res = await asyncHttpRequest(requestConfig);
	log.info('resolve asyncQnA');
	return res
};

const handleSuccess = (response) => {
	console.log('*******************SUCCESS:');
	console.log('data:');
	console.log(response.data);
	console.log('status:');
	console.log(response.status);
	console.log('statusText:');
	console.log(response.statusText);
	console.log('headers:');
	console.log(response.headers);
	// console.log('config:');
	// console.log(response.config);
}


const handleError = (error) => {
	console.log('*******************ERROR:');
	if (error.response) {
		// The request was made and the server responded with a status code
		// that falls out of the range of 2xx
		console.log(error.response.data);
		console.log(error.response.status);
		console.log(error.response.headers);
	} else if (error.request) {
		// The request was made but no response was received
		// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
		// http.ClientRequest in node.js
		console.log(error.request);
	} else {
		// Something happened in setting up the request that triggered an Error
		console.log('Error', error.message);
	}
	// console.log(error.config);
	process.exit(1);
}


export default {
	asyncSpellchecking: asyncSpellchecking,
	asyncEntityExtraction: asyncEntityExtraction,
	asyncQnA: asyncQnA,
	handleSuccess: handleSuccess,
	handleError: handleError
}
