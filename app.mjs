
import logging from 'simple-node-logger';
import restify from 'restify';
import builder from 'botbuilder';

import { config } from './config';
import api from './api';
import dic from './dictionary';


const log = logging.createSimpleLogger();

const connector = new builder.ChatConnector({
	appId: config.MicrosoftAppId,
	appPassword: config.MicrosoftAppPassword,
});
const bot = new builder.UniversalBot(connector);

const server = restify.createServer({ name: 'bot-qna-iC' });
server.listen(3987, () => {
	log.info(`${server.name} listening to ${server.url}`);
});

server.post('/api/messages', connector.listen());

log.info('Start server');


bot.dialog('/', session => {
	// Root dialog

	let text = dic.strip(session.message.text);
	log.info(text);
	if (dic.isGreetings(text)) {
		// greetings
		log.info('in Greetings');
		session.send(dic.BOT_SAYS_HI);
	} else if (dic.isGoodbye(text)) {
		// bye
		log.info('in Goodbye');
		session.beginDialog('getFeedback');
	} else {
		// QnA
		log.info('in QnA');
		log.info(text);

		api.asyncSpellchecking(text, dic.KEY)
			.then(response => {
				log.info('after spellchecking service');
				api.handleSuccess(response);
				let data = response.data,
					sentence;

				if (data.correctSentence) {
					sentence = data.correctSentence;
				} else {
					sentence = data.originalSentence;
				};
				log.info('sentence: ', sentence);
				api.asyncQnA(sentence)
					.then(response => {
						log.info('after QnA service');
						api.handleSuccess(response);
						let data = response.data;

						if (data.length === 1) {
							// only one intent returned, no ambiguity
							log.info('after QnA service - data.length=1 - one answer');
							let botText = data[0].answer;
							session.send(botText);
						} else if (data.length > 1) {
							// several intents returned, enter 'ambiguousDialog' dialog
							log.info('after QnA service - data.length>1 - several answers');
							session.beginDialog('ambiguousQuestion-2', data);
						} else {
							// zero intent returned, fallback message
							log.info('after QnA service - data.length<1 - No answer');
							let botText = dic.FALLBACK_MESSAGE;
							session.send(botText);
						}
					});
			});
	}
});


bot.dialog('ambiguousQuestion-1', (session, data) => {
	log.info('in  ambiguousQuestion');
	if (data) {
		// called from root dialog
		log.info('in ambiguousQuestion - coming');

		const message = new builder.Message(session)
			.text(dic.BOT_ASKS_FOR_SELECT_ACTION)
			.suggestedActions(
				builder.SuggestedActions.create(
					session,
					data.map(item =>
						builder.CardAction.postBack(
							session,
							`${dic.BOT_RECALLS_USER_QUESTION} "${item.questions[0]}"\n\n --- \n\n${item.answer}`,
							item.questions[0],
						)
					)
				)
			);
		session.send(message);
	}
	else {
		// called from itself
		log.info('in  ambiguousQuestion - leaving');
		session.send(session.message.text);
		session.endDialog();
	}
});


bot.dialog('ambiguousQuestion-2', [
	(session, data) => {
		log.info('in ambiguousQuestion - 1');
		let obj = {};
		for (let item of data) {
			obj[item.questions[0]] = {
				answer: item.answer
			};
		}
		session.dialogData.objData = obj;
		// session.save();

		builder.Prompts.choice(
			session,
			`${dic.BOT_ASKS_FOR_SELECT_ACTION}`,
			obj,
			{ listStyle: builder.ListStyle.list });
	},
	(session, results) => {
		log.info('in ambiguousQuestion - 2');
		let response = results.response;
		let obj = session.dialogData.objData;
		let text = `${dic.BOT_RECALLS_USER_QUESTION} "${response.entity}"\n\n --- \n\n${obj[response.entity].answer}`;
		session.send(text);
		session.endDialog();
	}]
);


bot.dialog('getFeedback', [
	(session) => {
		log.info('in getFeedback - 1');
		builder.Prompts.choice(
			session,
			`${dic.BOT_ASKS_FOR_FEEDBACK}`,
			"Yes|No",
			{ listStyle: builder.ListStyle.list });
	},
	(session, results) => {
		log.info('in getFeedback - 2');
		session.endDialog(dic.BOT_THANKS_FOR_FEEDBACK);
		session.endConversation();
	}]
);

