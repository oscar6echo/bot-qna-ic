# Q&A iC Chatbot

## 1 - Install and Run

From terminal:

```bash
# download
git clone https://gitlab.com/oscar6echo/bot-qna-ic.git

# move into directory
cd bot-qna-ic

# set the .env file - see doc

# run bot
npm start

# open emulator - see doc
```

### 1.1 - Data

+ Run the [notebook](https://nbviewer.jupyter.org/urls/gitlab.com/oscar6echo/bot-qna-ic/raw/master/bot-ic.ipynb) to transform the data from Excel format to json.  

### 1.2 - Botfuel

+ Sign up to [botfuel.io](https://app.botfuel.io/signup)
+ Create and app (it's free)
+ Rename the `?env_template` file to `.env`
+ Copy/paste the `App Id` and `App Key` to the `.env` file


### 1.3 - Bot

+ You need node version 8.5.0+ to [have ES6 modules natively](http://2ality.com/2017/09/native-esm-node.html).
+ Install bot with `npm install`
+ Launch bot with `npm start`

### 1.4 - Proxy

+ If necessary set your proxy host/port/login/password in the `.env` file.

> **WARNING**:  
As of mar18 the library **axios** needs be patched to work with a proxy.  
Cf. [axios PR#1232](https://github.com/axios/axios/pull/1232).


### 1.5 - Bot Framework Emulator

+ Install Emulator as explained on the [Microsoft Bot Framework page](https://docs.microsoft.com/en-us/bot-framework/debug-bots-emulator)
+ Open the emulator and set the address to `http://localhost:3987/api/messages`


## 2 - Structure

Dialogs:
1. Root Dialog: 3 cases
    + Greetings
    + Goodbye
    + Q&A
1. Ambiguous Question 1 - using [CardAction.postBack](https://docs.microsoft.com/en-us/bot-framework/nodejs/bot-builder-nodejs-send-suggested-actions)
1. Ambiguous Question 2 - using [Prompts.choice](https://docs.microsoft.com/en-us/bot-framework/nodejs/bot-builder-nodejs-dialog-prompt#promptschoice) (better)
1. Feedback

## 3 - Building blocks

This bot is based on the [Bot Microsoft Framework open source node SDK](https://docs.microsoft.com/en-us/azure/bot-service/nodejs/bot-builder-nodejs-quickstart).  
As a consequence you can test it with the [Bot Framework emulator](https://github.com/Microsoft/BotFramework-Emulator).


It uses [botfuel NLP services](https://app.botfuel.io/docs) spell checking and Q&A.  
To test them just git clone and run [this repo](https://gitlab.com/oscar6echo/botfuel-services).
